# Copyright 2008 Richard Brown
# Distributed under the terms of the GNU General Public License v2

require multibuild systemd-service flag-o-matic \
        lua [ whitelist='5.1' multibuild=false with_opt=true ]

SUMMARY="Lightweight, fast, secure, compliant, flexible,  web-server"
DESCRIPTION="A secure, fast, compliant and very flexible web-server
which has been optimized for high-performance environments. It has a very
low memory footprint compared to other webservers and takes care of cpu-load.
Its advanced feature-set (FastCGI, CGI, Auth, Output-Compression,
URL-Rewriting and many more) make lighttpd the perfect webserver-software
for every server that is suffering load problems."
HOMEPAGE="http://www.lighttpd.net"
DOWNLOADS="http://download.lighttpd.net/lighttpd/releases-$(ever range 1-2).x/${PNV}.tar.gz"

UPSTREAM_CHANGELOG="http://redmine.lighttpd.net/repositories/entry/lighttpd/tags/${PNV}/NEWS"
UPSTREAM_DOCUMENTATION="http://redmine.lighttpd.net/wiki/lighttpd"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="debug fam gdbm lua pcre ssl
    libunwind [[ description = [ Include libunwind support for backtraces on assert failures ] ]]
    webdav    [[ description = [ Enables webdev properties ] ]]
    xattr     [[ description = [ Enable extended attributes support ] ]]

    ssl? ( ( providers: libressl openssl ) [[ number-selected = exactly-one ]] )
"
DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/libev
        libunwind? ( dev-libs/libunwind )
        user/${PN}
        group/${PN}
        xattr? ( sys-apps/attr )
        debug? ( dev-util/valgrind )
        fam? ( app-admin/gamin )
        gdbm? ( sys-libs/gdbm )
        pcre? ( dev-libs/pcre )
        ssl? (
            providers:libressl? ( dev-libs/libressl:= )
            providers:openssl? ( dev-libs/openssl )
        )
        webdav? (
            dev-db/sqlite:3
            dev-libs/libxml2
            sys-fs/e2fsprogs
        )
"

#FIXME missing packages
#FIXME test would like fcgi

DEFAULT_SRC_CONFIGURE_PARAMS=(
    "--libdir=/usr/$(exhost --target)/lib/${PN}"
    '--enable-ipv6'
    '--without-mysql'
    '--without-ldap'
    '--without-kerberos5'
    '--without-memcache'
    '--with-libev'
    '--with-zlib'
    '--with-bzip2'
)

DEFAULT_SRC_CONFIGURE_OPTIONS=(
    "pcre PCRECONFIG=/usr/$(exhost --target)/bin/pcre-config"
)

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    "debug valgrind"
    fam
    gdbm
    libunwind
    lua
    pcre
    "ssl openssl"
    "webdav webdav-locks"
    "webdav webdav-props"
    "xattr attr"
)

pkg_setup() {
    exdirectory --allow /srv
}

src_configure() {
    CC_FOR_BUILD="$(exhost --build)-cc" \
    CFLAGS_FOR_BUILD="$(print-build-flags CFLAGS)" \
    CPPFLAGS_FOR_BUILD="$(print-build-flags CPPFLAGS)" \
    LDFLAGS_FOR_BUILD="$(print-build-flags LDFLAGS)" \
        default
}

src_install() {
    default

    insinto /etc/lighttpd/conf.d
    doins doc/config/conf.d/*.conf
    insinto /etc/lighttpd
    doins doc/config/*.conf
    edo sed -e '/^#server.pid-file[ \t]*=/s:^#::' \
            -e '/^#server.username[ \t]*=/{s:^#::;s:\"wwwrun\":\"lighttpd\":;}' \
            -e '/^#server.groupname[ \t]*=/{s:^#::;s:\"wwwrun\":\"lighttpd\":;}' \
            -i "${IMAGE}"/etc/lighttpd/lighttpd.conf

    dodoc doc/config/vhosts.d/vhosts.template

    keepdir /var/log/lighttpd
    keepdir /srv/www/htdocs

    edo chown lighttpd:lighttpd "${IMAGE}"/var/log/lighttpd
    edo chmod 0750 "${IMAGE}"/var/log/lighttpd

    insinto "${SYSTEMDSYSTEMUNITDIR}"
    doins doc/systemd/lighttpd.service
}

