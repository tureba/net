# Copyright 2013 Pierre Lejeune <superheron@gmail.com>
# Copyright 2014-2018 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ tag=${PV/_/-} ] cmake [ api=2 ]

SUMMARY="FreeRDP: A Remote Desktop Protocol implementation"
DESCRIPTION="
FreeRDP is a free implementation of the Remote Desktop Protocol (RDP), released under the Apache license.
"
HOMEPAGE+=" https://www.freerdp.com"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    aac
    alsa
    cups
    ffmpeg
    gstreamer
    kerberos
    mp3
    pcsc [[ description = [ Support for smartcard authentification via pcsc-lite ] ]]
    pulseaudio
    server [[ description = [ Build the FreeRDP server ] ]]
    systemd
    wayland
    ffmpeg? ( ( providers: ffmpeg libav ) [[ number-selected = exactly-one ]] )
    kerberos? ( ( providers: heimdal krb5 ) [[ number-selected = exactly-one ]] )
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        app-text/docbook-xsl-stylesheets
        dev-libs/libxslt
        virtual/pkg-config
    build+run:
        sys-libs/zlib
        x11-libs/libX11
        x11-libs/libXcursor
        x11-libs/libXext
        x11-libs/libXfixes
        x11-libs/libXi
        x11-libs/libXinerama
        x11-libs/libXrender
        x11-libs/libXv
        x11-libs/libxkbfile
        aac? (
            media-libs/faac
            media-libs/faad2
        )
        alsa? ( sys-sound/alsa-lib )
        cups? ( net-print/cups )
        ffmpeg? (
            providers:ffmpeg? ( media/ffmpeg )
            providers:libav? ( media/libav )
        )
        gstreamer? (
            media-libs/gstreamer:1.0
            media-plugins/gst-plugins-base:1.0
            x11-libs/libXrandr
        )
        kerberos? (
            providers:heimdal? ( app-crypt/heimdal )
            providers:krb5? ( app-crypt/krb5 )
        )
        mp3? ( media-sound/lame )
        pcsc? ( sys-apps/pcsc-lite )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
        pulseaudio? ( media-sound/pulseaudio )
        server? (
            x11-libs/libXdamage
            x11-libs/libXrandr
        )
        systemd? ( sys-apps/systemd )
        wayland? (
            sys-libs/wayland
            x11-libs/libxkbcommon
        )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/8b8e85271c4824e11b6d82ff9fc9b3fb664a8942.patch
    "${FILES}"/dbd630a38cb69ea027473b411a135d97c3da6096.patch
)

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILTIN_CHANNELS:BOOL=FALSE
    -DWITH_CCACHE:BOOL=FALSE
    -DWITH_CHANNELS:BOOL=TRUE
    -DWITH_CLIENT:BOOL=TRUE
    -DWITH_CLIENT_CHANNELS:BOOL=TRUE
    -DWITH_CLIENT_COMMON:BOOL=TRUE
    -DWITH_DIRECTFB:BOOL=FALSE
    -DWITH_DSP_EXPERIMENTAL:BOOL=FALSE
    -DWITH_GPROF:BOOL=FALSE
    -DWITH_GSM:BOOL=FALSE
    -DWITH_GSTREAMER_0_10:BOOL=FALSE
    -DWITH_IPP:BOOL=FALSE
    -DWITH_JPEG:BOOL=TRUE
    -DWITH_MANPAGES:BOOL=TRUE
    -DWITH_MBEDTLS:BOOL=FALSE
    -DWITH_OPENH264:BOOL=FALSE
    -DWITH_OPENSLES:BOOL=FALSE
    -DWITH_OPENSSL:BOOL=TRUE
    -DWITH_X11:BOOL=TRUE
    -DWITH_X264:BOOL=FALSE
    -DWITH_XCURSOR:BOOL=TRUE
    -DWITH_XEXT:BOOL=TRUE
    -DWITH_XI:BOOL=TRUE
    -DWITH_XINERAMA:BOOL=TRUE
    -DWITH_XKBFILE:BOOL=TRUE
    -DWITH_XRENDER:BOOL=TRUE
    -DWITH_XSHM:BOOL=TRUE
    -DWITH_XV:BOOL=TRUE
    -DWITH_ZLIB:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTION_WITHS=(
    'aac FAAC'
    'aac FAAD2'
    'alsa ALSA'
    'cups CUPS'
    'ffmpeg DSP_FFMPEG'
    'ffmpeg FFMPEG'
    'gstreamer GSTREAMER_1_0'
    'kerberos GSSAPI'
    'mp3 LAME'
    'pcsc PCSC'
    'pulseaudio PULSE'
    'server SERVER'
    'systemd LIBSYSTEMD'
    'wayland WAYLAND'
)
CMAKE_SRC_CONFIGURE_TESTS=(
    '-DBUILD_TESTING:BOOL=TRUE -DBUILD_TESTING:BOOL=FALSE'
)

src_test() {
    esandbox allow_net "unix:${TEMP%/}/.pipe/*"
    default
    esandbox disallow_net "unix:${TEMP%/}/.pipe/*"
}

