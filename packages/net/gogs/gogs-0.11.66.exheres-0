# Copyright 2017-2018 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=gogs tag=v${PV} ] \
    systemd-service [ systemd_files=[ scripts/systemd/gogs.service ] ]

SUMMARY="A painless self-hosted Git service"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"

# require additional go packages
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-lang/go[>=1.7]
    build+run:
        user/gogs
        group/gogs
        dev-db/sqlite:3
        sys-libs/pam
    run:
        dev-scm/git[>=1.7.1]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-0.11.19-adjust-config.patch
    "${FILES}"/${PN}-0.11.19-adjust-service-file.patch
)

src_prepare() {
  default

  export GOROOT="/usr/$(exhost --target)/lib/go"
  export GOPATH="${WORKBASE}"/build

  edo mkdir -p "${WORKBASE}"/build/src/github.com/gogs
  edo ln -s "${WORK}" "${WORKBASE}"/build/src/github.com/gogs/gogs
}

src_compile() {
    edo pushd "${WORKBASE}"/build/src/github.com/gogs/gogs

    edo go fix
    edo go build \
        -o bin/gogs \
        -tags='production sqlite pam cert'

    edo popd
}

src_install() {
    dobin "${WORKBASE}"/build/src/github.com/gogs/gogs/bin/gogs

    insinto /usr/share/gogs
    doins -r {conf,public,templates}

    insinto /etc/gogs
    doins conf/app.ini
    edo chown -R gogs:gogs "${IMAGE}"/etc/gogs

    keepdir /var/{cache,lib,log}/gogs
    edo chown gogs:gogs "${IMAGE}"/var/{cache,lib,log}/gogs

    install_systemd_files
}

